package com.yasser.medicuscompose

import com.yasser.medicuscompose.data.biomarkers.BiomarkerRepository
import com.yasser.medicuscompose.data.biomarkers.RealBiomarkerRepository
import com.yasser.medicuscompose.data.biomarkers.source.local.BiomarkerLocalDataSource
import com.yasser.medicuscompose.biomarkers.source.local.FakeBiomarkerLocalDataSource
import com.yasser.medicuscompose.data.biomarkers.source.remote.BiomarkerRemoteDataSource
import com.yasser.medicuscompose.biomarkers.source.remote.FakeBiomarkerRemoteDataSource
import com.yasser.medicuscompose.di.BiomarkerModule
import dagger.Binds
import dagger.Module
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import javax.inject.Singleton

@Module
@TestInstallIn(components = [SingletonComponent::class], replaces = [BiomarkerModule::class])
abstract class FakeBiomarkerModule {

    @Singleton @Binds
    abstract fun bindBiomarkerFakeRemoteDataSource(
        realBiomarkerRemoteDataSource: FakeBiomarkerRemoteDataSource
    ): BiomarkerRemoteDataSource

    @Singleton @Binds
    abstract fun bindBiomarkerFakeLocalDataSource(
        realBiomarkerLocalDataSource: FakeBiomarkerLocalDataSource
    ): BiomarkerLocalDataSource

    @Singleton @Binds
    abstract fun bindBiomarkerRepositoryReal(
        realBiomarkerRepository: RealBiomarkerRepository
    ): BiomarkerRepository

}