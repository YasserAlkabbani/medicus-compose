package com.yasser.medicuscompose

import androidx.activity.compose.setContent
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.navigation.compose.ComposeNavigator
import androidx.navigation.testing.TestNavHostController
import com.yasser.medicuscompose.manager.NavigationManager

import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
class InstrumentedTest {

    @get:Rule(order = 0)
    var hiltRule=HiltAndroidRule(this)

    @get:Rule(order=1)
    val composeAndroidTestRule= createAndroidComposeRule<MainActivity>()

    lateinit var navHostController:TestNavHostController

    @Before
    fun initAndSetNavHost() {
        hiltRule.inject()
        composeAndroidTestRule.activity.setContent {

            navHostController= TestNavHostController(LocalContext.current)

            navHostController.navigatorProvider.addNavigator(ComposeNavigator())

            MainContent(navHostController)

        }
    }

    @Test
    fun verifyStartDestination(){

        composeAndroidTestRule.onNodeWithTag("SCAFFOLD_TEST_TAG").assertIsDisplayed()
        composeAndroidTestRule.onNodeWithTag("LAZY_COLUMN_TEST_TAG").assertIsDisplayed()
        composeAndroidTestRule.onNodeWithTag("LAZY_COLUMN_TEST_TAG").performScrollToKey(5)
        composeAndroidTestRule.onNodeWithTag("LAZY_COLUMN_TEST_TAG").performScrollToKey(0)
        composeAndroidTestRule.onNodeWithTag("BIOMARKER_ITEM_TEST_TAG_3").assertIsDisplayed()
        val firstRoute=navHostController.currentBackStackEntry?.destination?.route
        Assert.assertEquals(firstRoute, NavigationManager.BiomarkersList.route)
        composeAndroidTestRule.onNodeWithTag("BIOMARKER_ITEM_TEST_TAG_3").performClick()

        val secondRoute=navHostController.currentBackStackEntry?.destination?.route
        Assert.assertEquals(secondRoute, "${NavigationManager.BiomarkerDetails.route}/{${NavigationManager.BiomarkerDetails.biomarkerId}}")
        composeAndroidTestRule.onNodeWithTag("BIOMARKER_DETAILS_TEST_TAG").assertIsDisplayed()
        composeAndroidTestRule.onNodeWithTag("BIOMARKER_INFO_ICON_TEST_TAG").assertIsDisplayed()
        composeAndroidTestRule.onNodeWithTag("BIOMARKER_INFO_ICON_TEST_TAG").performClick()
        composeAndroidTestRule.onNodeWithTag("BIOMARKER_INFO_DIALOG_TEST_TAG").assertIsDisplayed()
        composeAndroidTestRule.onNodeWithTag("BIOMARKER_INFO_ICON_TEST_TAG").performClick()
        composeAndroidTestRule.onNodeWithTag("BACK_BUTTON_TEST_TAG").assertIsDisplayed()
        composeAndroidTestRule.onNodeWithTag("BACK_BUTTON_TEST_TAG").performClick()
        composeAndroidTestRule.onNodeWithTag("LAZY_COLUMN_TEST_TAG").assertIsDisplayed()

    }

}