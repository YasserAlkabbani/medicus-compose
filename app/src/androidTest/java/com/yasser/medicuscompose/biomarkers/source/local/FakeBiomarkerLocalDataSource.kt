package com.yasser.medicuscompose.biomarkers.source.local

import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDatabaseEntity
import com.yasser.medicuscompose.data.biomarkers.source.local.BiomarkerLocalDataSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update

import javax.inject.Inject

class FakeBiomarkerLocalDataSource @Inject constructor(): BiomarkerLocalDataSource {

    private val biomarkerList:MutableStateFlow<List<BiomarkerDatabaseEntity>> = MutableStateFlow(listOf())

    override suspend fun insertBiomarker(vararg biomarkerDatabaseEntity: BiomarkerDatabaseEntity) {
        biomarkerList.update {oldList->
            oldList.toMutableList().apply {
                biomarkerDatabaseEntity.forEach {newItem->
                    val itemIndex=oldList.indexOfFirst { it.id==newItem.id }
                    if (itemIndex<0) add(newItem) else this[itemIndex]=newItem
                }
            }
        }
    }

    override fun returnBiomarkerByIdAsFlow(biomarkerId: Int): Flow<BiomarkerDatabaseEntity?> {
        return biomarkerList.map { it.firstOrNull { it.id==biomarkerId } }
    }

    override fun returnBiomarkersListAsFlow(): Flow<List<BiomarkerDatabaseEntity>> {
        return biomarkerList
    }
}