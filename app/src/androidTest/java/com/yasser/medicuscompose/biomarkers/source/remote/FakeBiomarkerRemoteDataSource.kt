package com.yasser.medicuscompose.biomarkers.source.remote

import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerRemoteEntity
import com.yasser.medicuscompose.data.biomarkers.source.remote.BiomarkerRemoteDataSource
import javax.inject.Inject

class FakeBiomarkerRemoteDataSource @Inject constructor(): BiomarkerRemoteDataSource {


    private val biomarkerRemoteList:List<BiomarkerRemoteEntity> = buildList {
        repeat(20){ add(
            BiomarkerRemoteEntity(
                it,date="May 25, 2021 6:58 PM",
                info="There is no need to specially prepare for this test. Ensure that your healthcare provider knows about all of medications you are taking",
                color = listOf("#E64A19","#00BCD4").random(),
                value="100", symbol = "symbol",insight="Long-term high blood sugar levels can injure your small blood vessels",
                category = "Blood"
            )
        ) }
    }

    override suspend fun returnBiomarkersList(): List<BiomarkerRemoteEntity> {
        return biomarkerRemoteList
    }

    override suspend fun returnBiomarkerById(biomarkerId: Int): BiomarkerRemoteEntity {
        return biomarkerRemoteList.first { it.id==biomarkerId }
    }
}