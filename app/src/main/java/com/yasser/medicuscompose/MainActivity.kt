package com.yasser.medicuscompose

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.yasser.medicuscompose.manager.NavigationManager
import com.yasser.medicuscompose.manager.startDestination
import com.yasser.medicuscompose.ui.theme.MedicusComposeTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent { MedicusComposeTheme { MainContent() } }

    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainContent(navHostController:NavHostController = rememberNavController()) {
    val context= LocalContext.current
    val mainViewModel: MainViewModel = hiltViewModel()
    val mainUIState = mainViewModel.mainUIState
    val currentNavigation = navHostController.currentBackStackEntryAsState().value


    val currentNavigationManager = remember(currentNavigation) {
        when (currentNavigation?.destination?.route?.substringBefore("/")) {
            NavigationManager.BiomarkersList.route -> NavigationManager.BiomarkersList
            NavigationManager.BiomarkerDetails.route -> NavigationManager.BiomarkerDetails
            else -> null
        }
    }
    LaunchedEffect(key1 = Unit, block = {
        mainUIState.popup.filter { it }.collect {
            mainUIState.updatePopup(false)
            if (navHostController.currentDestination?.route!=NavigationManager.BiomarkersList.route)navHostController.popBackStack()
        }
    })

    Scaffold(
        modifier = Modifier.fillMaxSize().testTag("SCAFFOLD_TEST_TAG"),
        topBar = {
            SmallTopAppBar(
                title = { Text(text = currentNavigationManager?.label?.returnText(context).orEmpty()) },
                navigationIcon = {
                    Row {
                        AnimatedVisibility(currentNavigationManager is NavigationManager.BiomarkerDetails) {
                            IconButton(
                                modifier = Modifier.testTag("BACK_BUTTON_TEST_TAG"),
                                onClick = { mainViewModel.mainUIState.updatePopup(true) },
                                content = {
                                    Icon(
                                        imageVector = Icons.Default.ArrowBack,
                                        contentDescription = null
                                    )
                                }
                            )
                        }
                    }
                },
            )
        },
        content = {
            it.calculateTopPadding()
            Surface(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(top = 55.dp)
            ) {
                MedicusNavHost(navHostController = navHostController)
            }
        },
    )
}

@Composable
fun MedicusNavHost(navHostController: NavHostController) {
    NavHost(
        navController = navHostController,
        startDestination = startDestination.route,
        builder = {
            composable(
                route = NavigationManager.BiomarkersList.route,
                content = {
                    NavigationManager.BiomarkersList.ReturnContent(
                        navigateToBiomarkerDetails = {
                            navHostController.navigate(
                                "${NavigationManager.BiomarkerDetails.route}/$it",
                                builder = NavigationManager.BiomarkerDetails.navOptionBuilder
                            )
                        }
                    )
                },
            )
            composable(
                route = "${NavigationManager.BiomarkerDetails.route}/{${NavigationManager.BiomarkerDetails.biomarkerId}}",
                arguments = listOf(navArgument(NavigationManager.BiomarkerDetails.biomarkerId) {
                    type = NavType.StringType
                }),
                content = { NavigationManager.BiomarkerDetails.ReturnContact() },
            )
        }
    )
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MedicusComposeTheme { MainContent() }
}