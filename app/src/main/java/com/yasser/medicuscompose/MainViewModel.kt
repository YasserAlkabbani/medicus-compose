package com.yasser.medicuscompose

import androidx.lifecycle.ViewModel
import com.yasser.medicuscompose.manager.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor():ViewModel(){

    val mainUIState:MainUIState=MainUIState()

}

class MainUIState{
    private val _popup:MutableStateFlow<Boolean> = MutableStateFlow(false)
    val popup:StateFlow<Boolean> =_popup
    fun updatePopup(newPopup:Boolean){_popup.update { newPopup }}
}