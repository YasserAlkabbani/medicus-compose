package com.yasser.medicuscompose.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.yasser.medicuscompose.data.biomarkers.source.local.BiomarkerDao
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDatabaseEntity

@Database(entities = [BiomarkerDatabaseEntity::class], version = 1)
abstract class BiomarkersRoomDatabase:RoomDatabase() {
    abstract fun biomarkerDao():BiomarkerDao
}