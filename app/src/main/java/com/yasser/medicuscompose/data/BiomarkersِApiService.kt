package com.yasser.medicuscompose.data

import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerRemoteEntity
import retrofit2.http.GET
import retrofit2.http.Path

interface BiomarkersApiService {

    @GET("hZZ5j8/biomarkers")
    suspend fun returnBiomarkersList():List<BiomarkerRemoteEntity?>

    @GET("hZZ5j8/biomarkers/{id}")
    suspend fun returnBiomarkersById(@Path("id") biomarkerId:Int):BiomarkerRemoteEntity

}