package com.yasser.medicuscompose.data.biomarkers

import com.yasser.medicuscompose.data.biomarkers.source.local.BiomarkerLocalDataSource
import com.yasser.medicuscompose.data.biomarkers.source.remote.BiomarkerRemoteDataSource
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDomainModel
import com.yasser.medicuscompose.manager.ResultManager
import com.yasser.medicuscompose.manager.requestManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

interface BiomarkerRepository{

    suspend fun refreshBiomarkerList():ResultManager<Unit>
    suspend fun refreshBiomarkerById(biomarkerId:Int):ResultManager<Unit>

    fun returnBiomarkerLocallyByIdAsFlow(biomarkerId:Int):Flow<BiomarkerDomainModel>
    fun returnBiomarkerLocallyListAsFlow():Flow<List<BiomarkerDomainModel>>

}

@Singleton
class RealBiomarkerRepository @Inject constructor(
    private val biomarkerRemoteDataSource: BiomarkerRemoteDataSource,
    private val biomarkerLocalDataSource: BiomarkerLocalDataSource,
) : BiomarkerRepository {

    override suspend fun refreshBiomarkerList(): ResultManager<Unit> {
        return requestManager {
            biomarkerRemoteDataSource.returnBiomarkersList().mapNotNull { it.mapperToDatabaseEntity() }.toTypedArray().let {
                biomarkerLocalDataSource.insertBiomarker(*it)
            }
        }
    }

    override suspend fun refreshBiomarkerById(biomarkerId: Int): ResultManager<Unit> {
        return requestManager {
            biomarkerRemoteDataSource.returnBiomarkerById(biomarkerId).mapperToDatabaseEntity()?.let {
                biomarkerLocalDataSource.insertBiomarker(it)
            }
        }
    }

    override fun returnBiomarkerLocallyByIdAsFlow(biomarkerId: Int):Flow<BiomarkerDomainModel> {
        return biomarkerLocalDataSource.returnBiomarkerByIdAsFlow(biomarkerId).map { it?.mapperToDomainModel() }.filterNotNull().flowOn(Dispatchers.Default)
    }

    override fun returnBiomarkerLocallyListAsFlow():Flow<List<BiomarkerDomainModel>>{
        return biomarkerLocalDataSource.returnBiomarkersListAsFlow().map { it.map { it.mapperToDomainModel() } }.flowOn(Dispatchers.Default)
    }

}
