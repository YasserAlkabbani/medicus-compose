package com.yasser.medicuscompose.data.biomarkers.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@Entity(tableName = "biomarkers_table")
data class BiomarkerDatabaseEntity (
    @PrimaryKey val id: Int,
    val date: String,
    val info: String,
    val color: String,
    val value: Int,
    val symbol: String,
    val insight: String,
    val category: String
){
    fun mapperToDomainModel():BiomarkerDomainModel{
        return BiomarkerDomainModel(
            id =id , date = date, info = info, color=color,value=value,
            symbol=symbol, insight = insight,category=category
        )
    }
}