package com.yasser.medicuscompose.data.biomarkers.entities

import android.graphics.Color
import androidx.core.graphics.ColorUtils

data class BiomarkerDomainModel (
    val id: Int,
    val date: String,
    val info: String,
    val color:String,
    val value: Int,
    val symbol: String,
    val insight: String,
    val category: String
)

val BiomarkerDomainModelWithData:BiomarkerDomainModel= BiomarkerDomainModel(
        -1,date="May 25, 2021 6:58 PM",
        info="There is no need to specially prepare for this test. Ensure that your healthcare provider knows about all of medications you are taking",
        color = "#AAAAAAAA",
        value=100, symbol = "symbol",insight="Long-term high blood sugar levels can injure your small blood vessels",
        category = "Blood"
    )
val BiomarkerDomainModelWithoutData:BiomarkerDomainModel= BiomarkerDomainModel(
    -1,date="", info="", color = "#AAAAAAAA", value=0, symbol = "",insight="", category = ""
)