package com.yasser.medicuscompose.data.biomarkers.entities

import android.graphics.Color
import androidx.core.graphics.ColorUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

data class BiomarkerRemoteEntity (
    val id: Int?,
    val date: String?,
    val info: String?,
    val color: String?,
    val value: String?,
    val symbol: String?,
    val insight: String?,
    val category: String?
){
    fun mapperToDatabaseEntity():BiomarkerDatabaseEntity?{
        return id?.let {
            BiomarkerDatabaseEntity(
                id = id, date = date?:"EMPTY DATA", info = info?:"EMPTY DATA",
                color = color?:"#AAAAAAAA" ,
                value=value?.toIntOrNull()?:0,symbol=symbol?:"EMPTY",
                insight = insight?:"EMPTY DATA", category = category?:"EMPTY DATA"
            )
        }
    }
}