package com.yasser.medicuscompose.data.biomarkers.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDatabaseEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface BiomarkerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBiomarker(vararg biomarkerDatabaseEntity: BiomarkerDatabaseEntity)

    @Query("SELECT * FROM biomarkers_table WHERE id= :biomarkerId")
    fun returnBiomarkerByIdAsFlow(biomarkerId:Int):Flow<BiomarkerDatabaseEntity?>

    @Query("SELECT * FROM biomarkers_table")
    fun returnBiomarkersListAsFlow():Flow<List<BiomarkerDatabaseEntity>>

}