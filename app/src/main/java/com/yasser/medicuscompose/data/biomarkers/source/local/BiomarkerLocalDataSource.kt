package com.yasser.medicuscompose.data.biomarkers.source.local

import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDatabaseEntity
import kotlinx.coroutines.flow.Flow
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

interface BiomarkerLocalDataSource {
    suspend fun insertBiomarker(vararg biomarkerDatabaseEntity: BiomarkerDatabaseEntity)
    fun returnBiomarkerByIdAsFlow(biomarkerId: Int): Flow<BiomarkerDatabaseEntity?>
    fun returnBiomarkersListAsFlow(): Flow<List<BiomarkerDatabaseEntity>>
}

@Singleton
class RealBiomarkerLocalDataSource @Inject constructor(private val biomarkerDao: BiomarkerDao) :
    BiomarkerLocalDataSource {

    override suspend fun insertBiomarker(vararg biomarkerDatabaseEntity: BiomarkerDatabaseEntity){
        biomarkerDao.insertBiomarker(*biomarkerDatabaseEntity)
    }

    override fun returnBiomarkerByIdAsFlow(biomarkerId:Int):Flow<BiomarkerDatabaseEntity?> =
        biomarkerDao.returnBiomarkerByIdAsFlow(biomarkerId)

    override fun returnBiomarkersListAsFlow(): Flow<List<BiomarkerDatabaseEntity>> =
        biomarkerDao.returnBiomarkersListAsFlow()
}