package com.yasser.medicuscompose.data.biomarkers.source.remote

import com.yasser.medicuscompose.data.BiomarkersApiService
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerRemoteEntity
import javax.inject.Inject
import javax.inject.Singleton

interface BiomarkerRemoteDataSource {
    suspend fun returnBiomarkersList():List<BiomarkerRemoteEntity>
    suspend fun returnBiomarkerById(biomarkerId:Int):BiomarkerRemoteEntity
}

@Singleton
class RealBiomarkerRemoteDataSource @Inject constructor(private val biomarkersApiService: BiomarkersApiService) :
    BiomarkerRemoteDataSource {
        override suspend fun returnBiomarkersList()=
            biomarkersApiService.returnBiomarkersList().filterNotNull()
        override suspend fun returnBiomarkerById(biomarkerId: Int) =
            biomarkersApiService.returnBiomarkersById(biomarkerId)
    }