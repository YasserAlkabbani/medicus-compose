package com.yasser.medicuscompose.di

import com.yasser.medicuscompose.data.biomarkers.BiomarkerRepository
import com.yasser.medicuscompose.data.biomarkers.RealBiomarkerRepository
import com.yasser.medicuscompose.data.biomarkers.source.local.BiomarkerLocalDataSource
import com.yasser.medicuscompose.data.biomarkers.source.local.RealBiomarkerLocalDataSource
import com.yasser.medicuscompose.data.biomarkers.source.remote.BiomarkerRemoteDataSource
import com.yasser.medicuscompose.data.biomarkers.source.remote.RealBiomarkerRemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module @InstallIn(SingletonComponent::class)
abstract class BiomarkerModule {

    @Singleton @Binds
    abstract fun bindBiomarkerRemoteDataSourceReal(
        realBiomarkerRemoteDataSource:RealBiomarkerRemoteDataSource
    ):BiomarkerRemoteDataSource

    @Singleton @Binds
    abstract fun bindBiomarkerLocalDataSourceReal(
        realBiomarkerLocalDataSource:RealBiomarkerLocalDataSource
    ):BiomarkerLocalDataSource

    @Singleton @Binds
    abstract fun bindBiomarkerRepositoryReal(
        realBiomarkerRepository: RealBiomarkerRepository
    ):BiomarkerRepository

}