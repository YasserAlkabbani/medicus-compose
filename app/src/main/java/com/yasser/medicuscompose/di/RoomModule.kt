package com.yasser.medicuscompose.di

import android.app.Application
import androidx.room.Room
import com.yasser.medicuscompose.data.BiomarkersRoomDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module @InstallIn(SingletonComponent::class)
object RoomModule {

    @Singleton @Provides
    fun provideBiomarkersRoomDatabase(application: Application):BiomarkersRoomDatabase{
        return Room.databaseBuilder(application,BiomarkersRoomDatabase::class.java,"biomarkers_table").build()
    }

    @Singleton @Provides
    fun provideBiomarkerDao(biomarkersRoomDatabase: BiomarkersRoomDatabase)=biomarkersRoomDatabase.biomarkerDao()

}