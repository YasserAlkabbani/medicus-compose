package com.yasser.medicuscompose.manager

import androidx.compose.runtime.Composable
import androidx.navigation.NavOptionsBuilder
import com.yasser.medicuscompose.R
import com.yasser.medicuscompose.ui.screen.biomarkers_details.BiomarkerDetailsScreen
import com.yasser.medicuscompose.ui.screen.biomarkers_list.BiomarkerListScreen

sealed class NavigationManager(
    val label:TextManager, val route :String,
    val navOptionBuilder:NavOptionsBuilder.()->Unit,
) {
    object BiomarkersList:NavigationManager(
        label = R.string.report_list.asTextManager(), route = "biomarker_list", navOptionBuilder = {}
    ){
        @Composable fun ReturnContent(navigateToBiomarkerDetails:(Int)->Unit)=BiomarkerListScreen(navigateToBiomarkerDetails)
    }
    object BiomarkerDetails:NavigationManager(
        label = R.string.biomarkers_details.asTextManager(), route = "biomarker_details", navOptionBuilder = {launchSingleTop = true},
    ){
        @Composable fun ReturnContact()=BiomarkerDetailsScreen()
        val biomarkerId:String="biomarkerId"
    }
}

val startDestination = NavigationManager.BiomarkersList
