package com.yasser.medicuscompose.manager

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.withContext


suspend fun <T>requestManager(request :suspend ()->T):ResultManager<T>{

    return withContext(Dispatchers.Default){
        try {
           ResultManager.Success(request())
        }catch (t:Throwable){
            ResultManager.Failed(t)
        }
    }

}

sealed class ResultManager<out T>{
    class Success<R>(val data:R):ResultManager<R>()
    class Failed(val throwable: Throwable):ResultManager<Nothing>()
}

class LoadingManager{
    private val _loading: MutableStateFlow<Int> = MutableStateFlow(0)
    val loading: StateFlow<Int> = _loading
    suspend fun processWithLoading(process: suspend () -> Unit) {
        _loading.update { it + 1 }
        process()
        _loading.update { it - 1 }
    }
}