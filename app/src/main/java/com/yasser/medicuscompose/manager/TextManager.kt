package com.yasser.medicuscompose.manager

import android.content.Context
import androidx.annotation.StringRes

sealed class TextManager {
    class StringText(val text:String):TextManager()
    class ResourceText(@StringRes val textRes:Int):TextManager()
    fun returnText(context: Context)=when(this){
        is ResourceText -> context.getString(textRes)
        is StringText -> text
    }
}
fun String.asTextManager()=TextManager.StringText(this)
fun Int.asTextManager()=TextManager.ResourceText(this)
