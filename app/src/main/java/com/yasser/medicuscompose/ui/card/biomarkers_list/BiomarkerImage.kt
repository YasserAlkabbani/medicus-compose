package com.yasser.medicuscompose.ui.card.biomarkers_list

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.graphics.ColorUtils

@Composable
fun BiomarkerImage(symbol:String, color:String){
    val strokeColor= remember(color) {
        try { android.graphics.Color.parseColor(color) }
        catch (t:Throwable){android.graphics.Color.parseColor("#AAAAAAAA")}
    }
    val containerColor= remember(strokeColor) {
        ColorUtils.blendARGB(strokeColor, android.graphics.Color.WHITE, 0.8f)
    }
    Surface(
        modifier = Modifier.size(80.dp),
        shape = CircleShape,
        border = BorderStroke(4.dp, Color(strokeColor)),
        color = Color(containerColor) ,
        contentColor = Color(strokeColor)
    ) {
        Box(Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            Text(
                text = symbol, style = MaterialTheme.typography.titleMedium,
                maxLines = 1
            )
        }
    }
}

@Preview
@Composable
private fun BiomarkerImagePreview1(){
    BiomarkerImage("HbA1C", "#E64A19")
}
@Preview
@Composable
private fun BiomarkerImagePreview2(){
    BiomarkerImage("HbA1C", "#00BCD4")
}


