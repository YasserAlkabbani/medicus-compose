package com.yasser.medicuscompose.ui.card

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDomainModel
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDomainModelWithData
import com.yasser.medicuscompose.ui.card.biomarkers_list.BiomarkerImage

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BiomarkerItemCard(
    modifier: Modifier,
    biomarkerDomainModel: BiomarkerDomainModel,
    onClick:(BiomarkerDomainModel)->Unit
){
    Surface(
        modifier=modifier.fillMaxWidth(),
        onClick = {onClick(biomarkerDomainModel)}
    ) {
        Row(
            modifier=Modifier.padding(10.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            BiomarkerImage(symbol = biomarkerDomainModel.symbol, color =biomarkerDomainModel.color)
            Spacer(modifier = Modifier.width(15.dp))
            Column(
                Modifier
                    .fillMaxWidth()
                    .weight(1f)) {
                Text(
                    modifier = Modifier.padding(2.dp),
                    text = biomarkerDomainModel.date, maxLines = 1
                )
                Text(
                    modifier = Modifier.padding(2.dp),
                    text = biomarkerDomainModel.value.toString(), maxLines = 1
                )
            }
            Icon(imageVector = Icons.Default.KeyboardArrowRight, contentDescription = null)
        }
    }
}


@Preview
@Composable
private fun BiomarkerItemCardPreview(){
    val biomarkerDomainModel:BiomarkerDomainModel= BiomarkerDomainModelWithData
    BiomarkerItemCard(Modifier,biomarkerDomainModel,{})
}