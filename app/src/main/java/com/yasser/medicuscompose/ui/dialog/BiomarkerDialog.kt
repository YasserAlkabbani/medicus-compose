package com.yasser.medicuscompose.ui.dialog

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun BiomarkerDialog(text:String){
    Surface(
        modifier = Modifier.fillMaxWidth().testTag("BIOMARKER_INFO_DIALOG_TEST_TAG"),
        shape = MaterialTheme.shapes.medium
    ) {
        Text(modifier = Modifier.padding(5.dp), text = text)
    }
}

@Preview
@Composable
fun BiomarkerDialogPreview(){
    BiomarkerDialog(text = "There is no need to specially prepare for this test. Ensure that your healthcare provider knows about all of medications you are taking")
}