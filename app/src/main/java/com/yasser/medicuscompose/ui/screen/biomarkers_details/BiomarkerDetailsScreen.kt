package com.yasser.medicuscompose.ui.screen.biomarkers_details

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.AlertDialog
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.yasser.medicuscompose.R
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDomainModel
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDomainModelWithoutData
import com.yasser.medicuscompose.manager.TextManager
import com.yasser.medicuscompose.ui.dialog.BiomarkerDialog
import com.yasser.medicuscompose.ui.slote.ErrorMessage
import com.yasser.medicuscompose.ui.slote.biomarker_details.BiomarkerDetailsData
import com.yasser.medicuscompose.ui.slote.biomarker_details.BiomarkerDetailsInfo
import kotlinx.coroutines.flow.MutableStateFlow

@Composable
fun  BiomarkerDetailsScreen() {

    val biomarkerDetailsViewModel:BiomarkerDetailsViewModel = hiltViewModel()
    val biomarkerDetailsUIState:BiomarkerDetailsUIState=biomarkerDetailsViewModel.biomarkerDetailsUIState
    val biomarkerDetailsViewModeEvent:BiomarkerDetailsViewModeEvent=biomarkerDetailsViewModel.biomarkerDetailsViewModeEvent

    val biomarkerInfoDialog:String?=biomarkerDetailsUIState.biomarkerInfoDialog.collectAsState().value
    if (biomarkerInfoDialog!=null){
        AlertDialog(
            onDismissRequest = {biomarkerDetailsUIState.updateDialogState(null)},
            title = {},
            text = { BiomarkerDialog(text = biomarkerInfoDialog) },
            confirmButton = {}
        )
    }

    BiomarkerDetailsScreenContent(
        biomarkerDetailsUIState = biomarkerDetailsUIState,
        biomarkerDetailsViewModeEvent =biomarkerDetailsViewModeEvent
    )
}

@Composable
fun  BiomarkerDetailsScreenContent(
    biomarkerDetailsUIState:BiomarkerDetailsUIState,
    biomarkerDetailsViewModeEvent:BiomarkerDetailsViewModeEvent
) {

    val biomarkerDomainModel:BiomarkerDomainModel=biomarkerDetailsUIState.biomarkerDomainModel.collectAsState().value
    val isRefreshing:Boolean=biomarkerDetailsUIState.loadingManager.loading.collectAsState().value>0
    val errorMessage:TextManager?=biomarkerDetailsUIState.errorMessage.collectAsState().value

    SwipeRefresh(
            modifier = Modifier.fillMaxSize(),
            state = rememberSwipeRefreshState(isRefreshing),
            onRefresh =biomarkerDetailsViewModeEvent.refreshBiomarkerDetails,
        ){
        Column(
            Modifier
                .fillMaxSize()
                .verticalScroll(rememberScrollState())) {
            AnimatedVisibility(errorMessage!=null) {
              ErrorMessage(
                  errorMessage = stringResource(id = R.string.connection_error),
                  onClick = biomarkerDetailsViewModeEvent.refreshBiomarkerDetails
              )
            }
            AnimatedVisibility(biomarkerDomainModel.id>=0) {
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(20.dp)
                    .wrapContentHeight()) {
                    BiomarkerDetailsData(biomarkerDomainModel =biomarkerDomainModel,biomarkerDetailsViewModeEvent.showDetailsDialog)
                    BiomarkerDetailsInfo(title = stringResource(R.string.info), subTitle = biomarkerDomainModel.info)
                    BiomarkerDetailsInfo(title = stringResource(R.string.insights), subTitle = biomarkerDomainModel.insight )
                }
            }
        }
    }
}

@Preview
@Composable
fun  BiomarkerDetailsScreenContentPreview() {
    BiomarkerDetailsScreenContent(
        BiomarkerDetailsUIState(MutableStateFlow(BiomarkerDomainModelWithoutData)),
        BiomarkerDetailsViewModeEvent({  },{})
    )
}