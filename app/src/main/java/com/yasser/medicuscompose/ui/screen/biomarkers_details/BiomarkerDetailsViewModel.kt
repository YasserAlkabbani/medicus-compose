package com.yasser.medicuscompose.ui.screen.biomarkers_details

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yasser.medicuscompose.R
import com.yasser.medicuscompose.data.biomarkers.BiomarkerRepository
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDomainModel
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDomainModelWithoutData
import com.yasser.medicuscompose.manager.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BiomarkerDetailsViewModel @Inject constructor(
    val savedStateHandle: SavedStateHandle, private val biomarkerRepository: BiomarkerRepository,
) : ViewModel() {

    val biomarkerId = savedStateHandle.get<String>(NavigationManager.BiomarkerDetails.biomarkerId)?.toIntOrNull()!!

    val biomarkerDetailsUIState = BiomarkerDetailsUIState(
        biomarkerRepository.returnBiomarkerLocallyByIdAsFlow(biomarkerId)
            .stateIn(viewModelScope, SharingStarted.Eagerly, BiomarkerDomainModelWithoutData)
    )
    val biomarkerDetailsViewModeEvent: BiomarkerDetailsViewModeEvent =
        BiomarkerDetailsViewModeEvent(
            showDetailsDialog = ::showBiomarkerDetailsDialog,
            refreshBiomarkerDetails = ::refreshBiomarkerDetails
        )

    private fun showBiomarkerDetailsDialog() {
        biomarkerDetailsUIState.updateDialogState(biomarkerDetailsUIState.biomarkerDomainModel.value.info)
    }

    private fun refreshBiomarkerDetails() {
        viewModelScope.launch {
            biomarkerDetailsUIState.loadingManager.processWithLoading {
                biomarkerDetailsUIState.updateErrorMessage(null)
                when(val processResult = biomarkerRepository.refreshBiomarkerById(biomarkerId)){
                    is ResultManager.Failed -> biomarkerDetailsUIState.updateErrorMessage(R.string.connection_error.asTextManager())
                    is ResultManager.Success -> {}
                }
            }
        }
    }

    init {
        refreshBiomarkerDetails()
    }

}


class BiomarkerDetailsViewModeEvent(
    val showDetailsDialog: () -> Unit, val refreshBiomarkerDetails: () -> Unit,
)

class BiomarkerDetailsUIState(
    val biomarkerDomainModel: StateFlow<BiomarkerDomainModel>
) {
    val loadingManager:LoadingManager=LoadingManager()

    private val _errorMessage: MutableStateFlow<TextManager?> = MutableStateFlow(null)
    val errorMessage: StateFlow<TextManager?> = _errorMessage
    fun updateErrorMessage(newErrorMessage:TextManager?){_errorMessage.update { newErrorMessage }}

    private val _biomarkerInfoDialog: MutableStateFlow<String?> = MutableStateFlow(null)
    val biomarkerInfoDialog: StateFlow<String?> = _biomarkerInfoDialog
    fun updateDialogState(showDialog: String?) {
        _biomarkerInfoDialog.update { showDialog }
    }
}