package com.yasser.medicuscompose.ui.screen.biomarkers_list

import androidx.compose.animation.*
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.yasser.medicuscompose.R
import com.yasser.medicuscompose.ui.card.BiomarkerItemCard
import com.yasser.medicuscompose.ui.slote.ErrorMessage
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull

@Composable
fun BiomarkerListScreen(navigateToBiomarkerDetails:(biomarkerId:Int)->Unit) {

    val biomarkerListViewModel:BiomarkerListViewModel = hiltViewModel()
    val biomarkerListUIState=biomarkerListViewModel.biomarkerListUIState
    val biomarkerViewModelEvent=biomarkerListViewModel.biomarkerViewModelEvent

    LaunchedEffect(key1 =Unit , block ={
        biomarkerListUIState.navigateToBiomarkerDetails.filterNotNull().collect{
                biomarkerListUIState.navigateToBiomarkerDetails(null)
                navigateToBiomarkerDetails(it)
            }
        }
    )


    BiomarkerListScreenContent(biomarkerListUIState,biomarkerViewModelEvent)
}

@OptIn(ExperimentalFoundationApi::class, ExperimentalAnimationApi::class)
@Composable
fun BiomarkerListScreenContent(
    biomarkerListUIState: BiomarkerListUIState, biomarkerViewModelEvent: BiomarkerViewModelEvent
) {
    val biomarkerList=biomarkerListUIState.biomarkerList.collectAsState().value
    val errorMessage=biomarkerListUIState.errorMessage.collectAsState().value
    val loading=biomarkerListUIState.loadingManager.loading.collectAsState().value>0


    SwipeRefresh(
        modifier = Modifier.fillMaxSize(),
        state = rememberSwipeRefreshState(loading),
        onRefresh =biomarkerViewModelEvent.refreshBiomarkerList,
    ){
         Column(Modifier.fillMaxSize()) {
            AnimatedVisibility(errorMessage!=null) {
                ErrorMessage(
                    errorMessage = stringResource(id = R.string.connection_error),
                    onClick = biomarkerViewModelEvent.refreshBiomarkerList
                )
            }
            AnimatedVisibility(biomarkerList.isNotEmpty()) {
                LazyColumn(
                    modifier = Modifier
                        .fillMaxSize()
                        .testTag("LAZY_COLUMN_TEST_TAG"),
                    contentPadding = PaddingValues(bottom = 50.dp),
//                verticalArrangement = Arrangement.spacedBy(10.dp),
                    content = {
                        itemsIndexed(
                            items = biomarkerList,
                            contentType ={index,value->"BIOMARKER"} ,
                            key ={index,value->value.id} ,
                            itemContent ={index,biomarkerDomainModel->
                                BiomarkerItemCard(
                                    modifier = Modifier
                                        .testTag("BIOMARKER_ITEM_TEST_TAG_$index")
                                        .animateItemPlacement()
                                        .animateEnterExit(
                                            enter = slideInVertically { it * (index + 1) },
                                            exit = slideOutVertically { it * (index + 1) }
                                        ) ,
                                    biomarkerDomainModel =biomarkerDomainModel ,
                                    onClick ={biomarkerListUIState.navigateToBiomarkerDetails(biomarkerDomainModel.id)}
                                )
                            } ,
                        )
                    }
                )
            }
        }
    }

}


@Preview
@Composable
fun BiomarkerListScreenContentPreview() {
    BiomarkerListScreenContent(
        BiomarkerListUIState(MutableStateFlow(listOf())),
        BiomarkerViewModelEvent({})
    )
}