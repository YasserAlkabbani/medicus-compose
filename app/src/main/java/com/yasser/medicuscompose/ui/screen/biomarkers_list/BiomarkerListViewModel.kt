package com.yasser.medicuscompose.ui.screen.biomarkers_list

import androidx.compose.runtime.collectAsState
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yasser.medicuscompose.MainViewModel
import com.yasser.medicuscompose.R
import com.yasser.medicuscompose.data.biomarkers.BiomarkerRepository
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDomainModel
import com.yasser.medicuscompose.manager.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BiomarkerListViewModel @Inject constructor(
    private val biomarkerRepository: BiomarkerRepository,
):ViewModel() {

    val biomarkerListUIState:BiomarkerListUIState=BiomarkerListUIState(biomarkerRepository.returnBiomarkerLocallyListAsFlow().stateIn(
        viewModelScope, SharingStarted.Eagerly,listOf(),
    ))
    val biomarkerViewModelEvent:BiomarkerViewModelEvent=BiomarkerViewModelEvent(refreshBiomarkerList = ::refreshBiomarkerList)

    private fun refreshBiomarkerList(){
        viewModelScope.launch {
            biomarkerListUIState.loadingManager.processWithLoading {
                biomarkerListUIState.updateErrorMessage(null)
                val result=biomarkerRepository.refreshBiomarkerList()
                when(result){
                    is ResultManager.Failed -> biomarkerListUIState.updateErrorMessage(R.string.connection_error.asTextManager())
                    is ResultManager.Success -> {}
                }
            }
        }
    }

    init {
        refreshBiomarkerList()
    }
}

class BiomarkerViewModelEvent(val refreshBiomarkerList:()->Unit)

class BiomarkerListUIState(val biomarkerList:StateFlow<List<BiomarkerDomainModel>>){

    val loadingManager: LoadingManager = LoadingManager()

    private val _errorMessage: MutableStateFlow<TextManager?> = MutableStateFlow(null)
    val errorMessage: StateFlow<TextManager?> = _errorMessage
    fun updateErrorMessage(newErrorMessage:TextManager?){_errorMessage.update { newErrorMessage }}

    private val _navigateToBiomarkerDetails:MutableStateFlow<Int?> = MutableStateFlow(null)
    val navigateToBiomarkerDetails:StateFlow<Int?> =_navigateToBiomarkerDetails
    fun navigateToBiomarkerDetails(biomarkerId:Int?){_navigateToBiomarkerDetails.update { biomarkerId }}

}
