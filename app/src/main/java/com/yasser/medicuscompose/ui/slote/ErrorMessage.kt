package com.yasser.medicuscompose.ui.slote

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.yasser.medicuscompose.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ErrorMessage(errorMessage:String, onClick:()->Unit) {
    Surface(
        modifier = Modifier.fillMaxWidth().testTag("ERROR_SURFACE_TEST_TAG"),
        onClick=    onClick,
        contentColor = MaterialTheme.colorScheme.error
    ) {
        Column(
            modifier = Modifier.padding(15.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Icon(imageVector = Icons.Default.Refresh, contentDescription =null )
            Spacer(modifier = Modifier.height(10.dp))
            Text(
                modifier = Modifier.testTag("ERROR_MESSAGE_TEST_TAG"),
                text = errorMessage,
                maxLines = 1,
                style = MaterialTheme.typography.titleLarge
            )
        }
    }
}


@Preview
@Composable
fun ErrorMessagePreview() {
    ErrorMessage(
        errorMessage = stringResource(id = R.string.connection_error),
        onClick = {}
    )
}