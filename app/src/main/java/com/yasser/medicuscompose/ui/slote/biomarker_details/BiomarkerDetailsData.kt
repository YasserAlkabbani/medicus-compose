package com.yasser.medicuscompose.ui.slote.biomarker_details

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.yasser.medicuscompose.R
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDomainModel
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDomainModelWithData
import com.yasser.medicuscompose.ui.card.biomarkers_list.BiomarkerImage

@Composable
fun BiomarkerDetailsData(
    biomarkerDomainModel: BiomarkerDomainModel,showDialog:()->Unit
){
    Column(
        Modifier.fillMaxWidth().testTag("BIOMARKER_DETAILS_TEST_TAG")) {
        Row() {
            Column(
                Modifier
                    .fillMaxWidth()
                    .weight(1f)) {
                Card(
                    modifier = Modifier
                ) {
                    Text(
                        modifier = Modifier.padding( horizontal = 10.dp),
                        text = biomarkerDomainModel.category,
                        maxLines = 1,
                    )
                }
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = biomarkerDomainModel.symbol,
                        style = MaterialTheme.typography.titleLarge,
                        maxLines = 1,
                    )
                    IconButton(
                        modifier = Modifier.testTag("BIOMARKER_INFO_ICON_TEST_TAG"),
                        onClick = showDialog
                    ) {
                        Icon(
                            imageVector = Icons.Default.Info,
                            tint = Color.LightGray,
                            contentDescription =null
                        )
                    }
                }
                Text(text = biomarkerDomainModel.date, maxLines = 1)
            }
            BiomarkerImage(
                symbol =biomarkerDomainModel.symbol ,
                color = biomarkerDomainModel.color
            )
        }
        val textColor= remember(biomarkerDomainModel.color) {
            Color(android.graphics.Color.parseColor(biomarkerDomainModel.color))
        }
        Text(
            modifier = Modifier.fillMaxWidth().padding(vertical = 30.dp),
            text = "${stringResource(R.string.your_result_is)} ${biomarkerDomainModel.value}",
            color = textColor,
            style = MaterialTheme.typography.titleLarge,
            textAlign = TextAlign.Center,
            maxLines = 1
        )
    }
}

@Preview
@Composable
fun BiomarkerDetailsDataPreview(){
    BiomarkerDetailsData(BiomarkerDomainModelWithData,{})
}