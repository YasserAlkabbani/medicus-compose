package com.yasser.medicuscompose.ui.slote.biomarker_details

import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun BiomarkerDetailsInfo(title:String,subTitle:String) {
    Column(
        Modifier.fillMaxWidth()) {
        Text(
            modifier = Modifier.fillMaxWidth().padding(15.dp), textAlign = TextAlign.Center,
            text = title, maxLines = 1, style = MaterialTheme.typography.titleLarge
        )
        ElevatedCard(
            modifier = Modifier.fillMaxWidth(),
            colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.surface),
        ) {
            Text(modifier = Modifier.padding(20.dp),text = subTitle)
        }
    }
}

@Preview
@Composable
fun BiomarkerDetailsInfoPreview(){
    BiomarkerDetailsInfo(
        title = "TITLE",
        subTitle ="There is no need to specially prepare for this test. Ensure that your healthcare provider knows about all of medications you are taking"
    )
}