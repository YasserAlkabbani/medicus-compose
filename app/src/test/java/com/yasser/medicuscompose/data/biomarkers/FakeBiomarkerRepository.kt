package com.yasser.medicuscompose.data.biomarkers

import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDatabaseEntity
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDomainModel
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerRemoteEntity
import com.yasser.medicuscompose.manager.ResultManager
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*

class FakeBiomarkerRepository:BiomarkerRepository {

    private var errorRequest:Boolean=false
    fun setErrorRequest(newErrorRequest:Boolean){errorRequest=newErrorRequest}

    private val biomarkerLocalList:MutableStateFlow<List<BiomarkerDatabaseEntity>> = MutableStateFlow(listOf())
    private val biomarkerRemoteList:List<BiomarkerRemoteEntity> = buildList {
        repeat(10){ add(
            BiomarkerRemoteEntity(
                it,date="May 25, 2021 6:58 PM",
                info="There is no need to specially prepare for this test. Ensure that your healthcare provider knows about all of medications you are taking",
                color = "#AAAAAAAA",
                value="100", symbol = "symbol",insight="Long-term high blood sugar levels can injure your small blood vessels",
                category = "Blood"
            )
        ) }
    }

    override suspend fun refreshBiomarkerList(): ResultManager<Unit> {
        delay(1000)
        return if (!errorRequest) {
            val newBiomarkerList=biomarkerRemoteList.mapNotNull { it.mapperToDatabaseEntity() }
            biomarkerLocalList.update { newBiomarkerList }
            ResultManager.Success(Unit)
        }else ResultManager.Failed(Throwable("ERROR"))
    }

    override suspend fun refreshBiomarkerById(biomarkerId: Int): ResultManager<Unit> {
        delay(1000)
        return if (!errorRequest){
            val freshBiomarker=biomarkerRemoteList.firstOrNull { it.id==biomarkerId }?.mapperToDatabaseEntity()
            val localBiomarkerList=biomarkerLocalList.value.toMutableList()
            if (freshBiomarker!=null){
                val oldBiomarkerIndex=biomarkerLocalList.value.indexOfFirst { it.id==biomarkerId }
                if (oldBiomarkerIndex>=0){
                    localBiomarkerList[oldBiomarkerIndex]=freshBiomarker
                }else{
                    localBiomarkerList.add(freshBiomarker)
                }
            }
            biomarkerLocalList.update { localBiomarkerList }
            ResultManager.Success(Unit)
        }else ResultManager.Failed(Throwable("ERROR"))

    }

    override fun returnBiomarkerLocallyByIdAsFlow(biomarkerId: Int): Flow<BiomarkerDomainModel> {
        return biomarkerLocalList.mapNotNull { it.firstOrNull { it.id==biomarkerId }?.mapperToDomainModel() }
    }

    override fun returnBiomarkerLocallyListAsFlow(): Flow<List<BiomarkerDomainModel>> {
        return biomarkerLocalList.map { it.map { it.mapperToDomainModel() } }
    }


}