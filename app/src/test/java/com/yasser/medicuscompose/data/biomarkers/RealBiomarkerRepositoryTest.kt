package com.yasser.medicuscompose.data.biomarkers

import com.yasser.medicuscompose.MainDispatcherRule
import com.yasser.medicuscompose.data.biomarkers.source.local.BiomarkerLocalDataSource
import com.yasser.medicuscompose.data.biomarkers.source.local.FakeBiomarkerLocalDataSource
import com.yasser.medicuscompose.data.biomarkers.source.remote.BiomarkerRemoteDataSource
import com.yasser.medicuscompose.data.biomarkers.source.remote.FakeBiomarkerRemoteDataSource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule

import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class RealBiomarkerRepositoryTest {

    private lateinit var fakeBiomarkerLocalDataSource: BiomarkerLocalDataSource
    private lateinit var fakeBiomarkerRemoteDataSource: BiomarkerRemoteDataSource
    private lateinit var realBiomarkerRepository: RealBiomarkerRepository

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun createBiomarkerRepository(){
        fakeBiomarkerLocalDataSource=FakeBiomarkerLocalDataSource()
        fakeBiomarkerRemoteDataSource=FakeBiomarkerRemoteDataSource()
        realBiomarkerRepository=
            RealBiomarkerRepository(fakeBiomarkerRemoteDataSource,fakeBiomarkerLocalDataSource)
    }

    @Test
    fun refreshBiomarkerList() = runTest(UnconfinedTestDispatcher()){
        assertEquals(realBiomarkerRepository.returnBiomarkerLocallyListAsFlow().first().size,0)
        realBiomarkerRepository.refreshBiomarkerList()
        assertEquals(realBiomarkerRepository.returnBiomarkerLocallyListAsFlow().first().size,10)
        assertEquals(realBiomarkerRepository.returnBiomarkerLocallyListAsFlow().first()[0].id,0)
        assertEquals(realBiomarkerRepository.returnBiomarkerLocallyListAsFlow().first()[2].id,2)
        assertEquals(realBiomarkerRepository.returnBiomarkerLocallyListAsFlow().first()[8].id,8)
    }

    @Test
    fun refreshBiomarkerById() = runTest(UnconfinedTestDispatcher()){
        realBiomarkerRepository.refreshBiomarkerList()
        assertEquals(realBiomarkerRepository.returnBiomarkerLocallyByIdAsFlow(5).first().id,5)
        assertEquals(realBiomarkerRepository.returnBiomarkerLocallyByIdAsFlow(8).first().id,8)
    }


}