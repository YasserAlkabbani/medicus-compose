package com.yasser.medicuscompose.data.biomarkers.source.local

import android.app.Application
import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.yasser.medicuscompose.data.BiomarkersRoomDatabase
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDatabaseEntity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException


@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(AndroidJUnit4::class)
class BiomarkerDatabaseTest {
    private lateinit var biomarkerDao: BiomarkerDao
    private lateinit var biomarkersRoomDatabase: BiomarkersRoomDatabase

    @Before
    fun createDatabase(){
        val context=ApplicationProvider.getApplicationContext<Context>()
        biomarkersRoomDatabase= Room.inMemoryDatabaseBuilder(context,BiomarkersRoomDatabase::class.java).build()
        biomarkerDao = biomarkersRoomDatabase.biomarkerDao()
    }
    @After
    @Throws(IOException::class)
    fun closeDatabase(){
        biomarkersRoomDatabase.close()
    }


    @Test
    @Throws(Exception::class)
    fun insertAndReadBiomarker() = runTest(UnconfinedTestDispatcher()){
        val savedBiomarkerDatabaseModelBeforeAdd=biomarkerDao.returnBiomarkerByIdAsFlow(1).firstOrNull()
        Assert.assertEquals(savedBiomarkerDatabaseModelBeforeAdd?.id,null)
        val biomarkerDatabaseEntity:BiomarkerDatabaseEntity=BiomarkerDatabaseEntity(
            1,"12/12/2022","TEST_DATABASE_INFO","#AAAAAAAA",100,
            "TEST_DATABASE_SYMBOL","TEST_DATABASE_INSIGHT","TEST_CATEGORY",
        )
        biomarkerDao.insertBiomarker(biomarkerDatabaseEntity)
        val savedBiomarkerDatabaseModelAfterAdd=biomarkerDao.returnBiomarkerByIdAsFlow(1).firstOrNull()
        Assert.assertEquals(savedBiomarkerDatabaseModelAfterAdd?.id,1)
    }


}