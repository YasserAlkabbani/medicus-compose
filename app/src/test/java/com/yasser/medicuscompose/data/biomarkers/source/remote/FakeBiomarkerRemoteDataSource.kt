package com.yasser.medicuscompose.data.biomarkers.source.remote

import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerDomainModel
import com.yasser.medicuscompose.data.biomarkers.entities.BiomarkerRemoteEntity
import kotlinx.coroutines.delay
import javax.inject.Inject
import javax.inject.Singleton

class FakeBiomarkerRemoteDataSource :BiomarkerRemoteDataSource {


    private val biomarkerRemoteList:List<BiomarkerRemoteEntity> = buildList {
        repeat(10){ add(
            BiomarkerRemoteEntity(
                it,date="May 25, 2021 6:58 PM",
                info="There is no need to specially prepare for this test. Ensure that your healthcare provider knows about all of medications you are taking",
                color = "123456789",
                value="100", symbol = "symbol",insight="Long-term high blood sugar levels can injure your small blood vessels",
                category = "Blood"
            )
        ) }
    }

    override suspend fun returnBiomarkersList(): List<BiomarkerRemoteEntity> {
        delay(1000)
        return biomarkerRemoteList
    }

    override suspend fun returnBiomarkerById(biomarkerId: Int): BiomarkerRemoteEntity {
        delay(1000)
        return biomarkerRemoteList.first { it.id==biomarkerId }
    }
}