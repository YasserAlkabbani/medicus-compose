package com.yasser.medicuscompose.ui.screen.biomarkers_details

import androidx.lifecycle.SavedStateHandle
import com.yasser.medicuscompose.MainDispatcherRule
import com.yasser.medicuscompose.data.biomarkers.FakeBiomarkerRepository
import com.yasser.medicuscompose.manager.NavigationManager
import com.yasser.medicuscompose.ui.screen.biomarkers_list.BiomarkerListViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule

import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class BiomarkerDetailsViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    lateinit var biomarkerDetailsViewModel: BiomarkerDetailsViewModel
    lateinit var fakeBiomarkerRepository:FakeBiomarkerRepository

    @Before
    fun initBiomarkerListViewModel(){
        val savedStateHandle=SavedStateHandle().apply { set(NavigationManager.BiomarkerDetails.biomarkerId,"1") }
        fakeBiomarkerRepository= FakeBiomarkerRepository()
        fakeBiomarkerRepository.setErrorRequest(true)
        biomarkerDetailsViewModel= BiomarkerDetailsViewModel(savedStateHandle,fakeBiomarkerRepository)

    }

    @Test
    fun saveHandelStateValue_WithValue_ReturnOne(){
        val biomarkerId=biomarkerDetailsViewModel.savedStateHandle.get<String>(NavigationManager.BiomarkerDetails.biomarkerId)?.toIntOrNull()!!
        assertEquals(biomarkerId,1)
    }

    @Test
    fun biomarkerById_RefreshData_Error() = runTest(UnconfinedTestDispatcher()) {
        assertEquals(biomarkerDetailsViewModel.biomarkerDetailsUIState.biomarkerDomainModel.value.id,-1)
        advanceUntilIdle()
        assertEquals(biomarkerDetailsViewModel.biomarkerDetailsUIState.biomarkerDomainModel.value.id,-1)
        assertNotEquals(biomarkerDetailsViewModel.biomarkerDetailsUIState.errorMessage.value,null)
    }

    @Test
    fun biomarkerById_RefreshData_Success() = runTest(UnconfinedTestDispatcher()) {
        fakeBiomarkerRepository.setErrorRequest(false)
        biomarkerDetailsViewModel.biomarkerDetailsViewModeEvent.refreshBiomarkerDetails()
        advanceUntilIdle()
        assertEquals(biomarkerDetailsViewModel.biomarkerDetailsUIState.biomarkerDomainModel.value.id,1)
        assertEquals(biomarkerDetailsViewModel.biomarkerDetailsUIState.errorMessage.value,null)
    }
}