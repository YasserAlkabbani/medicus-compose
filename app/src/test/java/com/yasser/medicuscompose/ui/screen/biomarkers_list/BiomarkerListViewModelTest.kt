package com.yasser.medicuscompose.ui.screen.biomarkers_list

import com.yasser.medicuscompose.MainDispatcherRule
import com.yasser.medicuscompose.data.biomarkers.FakeBiomarkerRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule

import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class BiomarkerListViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var biomarkerListViewModel:BiomarkerListViewModel
    private lateinit var fakeBiomarkerRepository:FakeBiomarkerRepository

    @Before
    fun initBiomarkerListViewModel(){
        fakeBiomarkerRepository=FakeBiomarkerRepository()
        fakeBiomarkerRepository.setErrorRequest(true)
        biomarkerListViewModel= BiomarkerListViewModel(fakeBiomarkerRepository)
    }

    @Test
    fun updateAndRefreshBiomarkerList_BiomarkerList_ZeroThenTen()= runTest(UnconfinedTestDispatcher()){

        advanceUntilIdle()
        assertEquals(biomarkerListViewModel.biomarkerListUIState.biomarkerList.value.size,0)

        assertNotEquals(biomarkerListViewModel.biomarkerListUIState.errorMessage.value,null)

        fakeBiomarkerRepository.setErrorRequest(false)
        biomarkerListViewModel.biomarkerViewModelEvent.refreshBiomarkerList()

        advanceUntilIdle()
        assertEquals(biomarkerListViewModel.biomarkerListUIState.biomarkerList.value.size,10)
        assertEquals(biomarkerListViewModel.biomarkerListUIState.errorMessage.value,null)

    }

    @Test
    fun updateAndRefreshBiomarkerList_LoadingState_ZeroThenOne()= runTest(UnconfinedTestDispatcher()){

        fakeBiomarkerRepository.setErrorRequest(false)
        biomarkerListViewModel.biomarkerViewModelEvent.refreshBiomarkerList()

        assertNotEquals(biomarkerListViewModel.biomarkerListUIState.loadingManager.loading.value,0)

        advanceUntilIdle()

        assertEquals(biomarkerListViewModel.biomarkerListUIState.loadingManager.loading.value,0)

    }

    @Test
    fun updateAndRefreshBiomarkerList_ResultState_Error()= runTest(UnconfinedTestDispatcher()){

        advanceUntilIdle()
        assertNotEquals(biomarkerListViewModel.biomarkerListUIState.errorMessage.value,null)

        fakeBiomarkerRepository.setErrorRequest(false)

        biomarkerListViewModel.biomarkerViewModelEvent.refreshBiomarkerList()

        advanceUntilIdle()

        assertEquals(biomarkerListViewModel.biomarkerListUIState.loadingManager.loading.value,0)

    }

}